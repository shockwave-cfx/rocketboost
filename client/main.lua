local INPUT_CHARACTER_WHEEL = 19
local INPUT_VEH_HORN = 86

local function IsBoostControlPressed()
  return (
    IsControlPressed(0, INPUT_VEH_HORN) or
    IsControlPressed(0, INPUT_CHARACTER_WHEEL)
  )
end

Citizen.CreateThread(function ()
  local function RocketLoop()
    local player = PlayerPedId()
    local vehicle = GetVehiclePedIsIn(player)

    if not CanPedUseVehicleRocket(player, vehicle) then
      return
    end

    local shouldBoost = IsBoostControlPressed()
    local isBoosting = IsVehicleInfiniteRocketBoostActive(vehicle)

    if shouldBoost and not isBoosting then
      SetVehicleInfiniteRocketBoostActive(vehicle, true)
      -- TriggerServerEvent('rocketboost:__sync', true, false)
    elseif not shouldBoost and isBoosting then
      SetVehicleInfiniteRocketBoostActive(vehicle, false)
      -- TriggerServerEvent('rocketboost:__sync', false, false)
    end
  end

  while true do
    RocketLoop()
    Citizen.Wait(0)
  end
end)
