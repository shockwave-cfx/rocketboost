local vehicles = {}

function IsVehicleInfiniteRocketBoostActive(vehicle)
  return vehicles[vehicle]
end

function SetVehicleInfiniteRocketBoostActive(vehicle, active)
  if IsVehicleInfiniteRocketBoostActive(vehicle) == active then
    return
  end

  SetVehicleRocketBoostActive(vehicle, active)
  vehicles[vehicle] = active or nil
end

Citizen.CreateThread(function ()
  local function RefillLoop()
    for vehicle in pairs(vehicles) do
      if not IsVehicleRocketBoostActive(vehicle) then
        SetVehicleRocketBoostActive(vehicle, true)
      end

      SetVehicleRocketBoostPercentage(vehicle, 100.0)
    end
  end

  while true do
    RefillLoop()
    Citizen.Wait(0)
  end
end)
